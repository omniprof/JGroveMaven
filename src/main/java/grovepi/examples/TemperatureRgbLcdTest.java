package grovepi.examples;

import grovepi.GrovePi;
import grovepi.Pin;
import grovepi.common.Delay;
import grovepi.sensors.TemperatureAndHumiditySensor;
import grovepi.i2c_devices.RgbLcdDisplay;

/**
 * Original version RgbLcdDisplayTest.java by Johannes Bergmann
 *
 * Modified to read the temperature sensor and display it on the LCD RGB display
 *
 * @author Ken Fogel
 * @version 0.1
 */
public class TemperatureRgbLcdTest {

    // To instantiate the TemperatureAndHumiditySensor you need a GrovePi object
    private final GrovePi grovePi;
    private final TemperatureAndHumiditySensor temperature;
    // But you don't need GrovePi for the RgbLcdDisplay
    private final RgbLcdDisplay lcd;

    /**
     * Constructor initializes
     */
    public TemperatureRgbLcdTest() {
        grovePi = new GrovePi();
        temperature = grovePi.getDeviceFactory().createTemperatureAndHumiditySensor(2);
        lcd = new RgbLcdDisplay();

        // The program is run from main and not the constructor
        //run();
    }

    /**
     * This is the method that actually does the work of retrieving the
     * temperature and displaying it
     */
    public void run() {
        lcd.display(true);
        lcd.setBacklightRgb(100, 100, 100);
        for (int x = 1; x < 6; ++x) {

            // Retrive the data from the sensor. Subscript 0 is the temperature
            // and subscript 1 is the humidity
            float[] value = temperature.update();
            String s = x + ". " + value[0] + " Celcius" + "\n";
            // Display the String in the display
            lcd.setText(s);
            // Approximately 1 second delay
            Delay.milliseconds(1000);
        }
        lcd.shutdown();
    }

    /**
     * Where it all begins
     *
     * @param args
     */
    public static void main(String[] args) {
        final TemperatureRgbLcdTest test = new TemperatureRgbLcdTest();
        test.run();
        System.exit(0);
    }
}
