# JGroveMaven

RaspberryPi+GrovePi Java library, licensed under the MIT License (MIT).


Initially based on the jgrove code from the "Captain Picar" project by Johannes Bergmann (jbman): https://github.com/jbman/captain-picar -- but mostly rewritten to add missing sensors and to be similar to the C# implementation of GrovePi: https://github.com/DexterInd/GrovePi.


Written by Dan Jackson, Newcastle University, 2015.
https://github.com/digitalinteraction/jgrove
Forked and revised by Ken Fogel July 2016


This version has been made into a Maven based project.


This project employs the technique found in https://www.omnijava.com/2016/07/17/how-to-run-maven-based-projects-on-a-remote-raspberry-pi-using-netbeans-part-2-of-2/ for a Windows development environment where Maven deploys and executes the program using WinSCP and a script file.


You will need to edit wintopi.txt to use your Pi's IP number, username and password.


If you do not want Maven to deploy and execute then comment out the exec-maven-plugin section of the pom.xml.


To run a specific example you will need to:

1) Open the file that matches the sensor that you are using and ensure that the correct pin is being used. On a GrovePi shield I have the temperature sensor plugged into D4 which means pin 4.

2) Adapt the appropriate grovepi.examples file for the sensor you wish to play with. Look at TemperatureRgbLcdTest.java where I have modified the original code to display the results on the Grove-LCD RGB Backlight module.


See my series of articles entitled IoT without the Breadboard. Part 3 of the series looks at this project.


